/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slang.word;

import java.io.Serializable;

/**
 *
 * @author Rokia
 */
public class SlangWord implements Serializable {
    private String ID;
    private String Description;
 
    public SlangWord(String ID, String Description) {
        super();
        this.ID = ID;
        this.Description = Description;
    }
 
    public String getID() {
        return ID;
    }
 
    public void setID(String ID) {
        this.ID = ID;
    }
 
    public String getDescription() {
        return Description;
    }
 
    public void setDescription(String Description) {
        this.Description = Description;
    }
    
    public String toString() {
        return "Key: " + this.ID + "\nDescription: " + this.Description + "\n";
    }
}