/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slang.word;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Rokia
 */
public class ManageSlangWord {

    private static final String slangWordHistory_FILENAME = "slangHistory.txt";
    private static final String slangWord_FILENAME = "slang.txt";
    private static final String slangWordRoot_FILENAME = "slangRoot.txt";
    public List<String> lstSlangWord = new ArrayList<String>();
    public TreeMap<String, List<String>> TMSlangWord = new TreeMap<String, List<String>>();

    public void read() {
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader brd = null;
        lstSlangWord = new ArrayList<String>();
        try {
            fis = new FileInputStream(new File(slangWord_FILENAME));
            isr = new InputStreamReader(fis);
            brd = new BufferedReader(isr);
            String line;
            brd.readLine();
            while ((line = brd.readLine()) != null) {
                lstSlangWord.add(line);
                String[] arrSlangWordObj = line.split("`", 0);
                int i = 0;
                String key = "";
                List<String> value = new ArrayList<String>();
                for (String word : arrSlangWordObj) {
                    if (i == 0) {
                        key = word;
                    }
                    if (i == 1) {
                        if (word.contains("|")) {
                            String[] arrDescription = word.split("\\|", 0);
                            for (String description : arrDescription) {
                                value.add(description);
                            }
                        } else {
                            value.add(word);
                        }
                    }
                    i++;
                }
                TMSlangWord.put(key, value);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                isr.close();
                fis.close();
                brd.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }

        }
    }

    public void save() {
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new File(slangWord_FILENAME));

            for (String k : TMSlangWord.keySet()) {
                if (k == null || TMSlangWord.get(k) == null) {
                    continue;
                }

                StringBuilder strContent = new StringBuilder();
                strContent.append(k + "`");
                int i = 0;
                for (String description : TMSlangWord.get(k)) {
                    if (i > 0) {
                        strContent.append("|" + description);
                    } else {
                        strContent.append(description);
                    }
                    ++i;
                }

                pw.println(strContent.toString());
            }

        } catch (Exception e) {
            System.err.println(e.getMessage());
        } finally {
            pw.flush();
            pw.close();
        }
    }

    public void saveHistory(Map<String, List<String>> maps) {
        FileWriter fw = null;
        File fileHistory = new File(slangWordHistory_FILENAME);
        try {
            fw = new FileWriter(fileHistory, true);
            for (String key : maps.keySet()) {
                StringBuilder str = new StringBuilder();
                int i = 0;
                for (String description : maps.get(key)) {
                    if (i > 0) {
                        str.append("|" + description);
                    } else {
                        str.append(description);
                    }
                    ++i;
                }
                fw.write(key + "`" + str + "\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException e) {
            }
        }
    }

    public Integer checkDuplicate(String key, Boolean isHistory) {
        Scanner scan = null;
        File fileName = null;
        if (isHistory) {
            fileName = new File(slangWordHistory_FILENAME);
        } else {
            fileName = new File(slangWord_FILENAME);
        }
        int lineNumber = 0;
        try {
            scan = new Scanner(fileName);
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                if (line.indexOf(key) != -1) {
                    return lineNumber;
                }
                ++lineNumber;
            }
        } catch (FileNotFoundException ex) {
            System.err.println(ex.getMessage());
        } finally {
            scan.close();
        }

        return lineNumber;
    }

    public void resetSlangWord() {
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(new File(slangWordRoot_FILENAME)).getChannel();
            destination = new FileOutputStream(new File(slangWord_FILENAME)).getChannel();
            destination.transferFrom(source, 0, source.size());
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        } finally {
            try {
                source.close();
                destination.close();
            } catch (IOException ex) {
            }
        }
    }

    public Map<String, List<String>> Search(String text, Boolean isKey) {
        if (text.isEmpty()) {
            return null;
        }

        Map<String, List<String>> maps = new HashMap<String, List<String>>();

        if (isKey) {
            maps = TMSlangWord.entrySet().stream().filter(data -> data.getKey().toLowerCase().indexOf(text.toLowerCase()) != -1).collect(Collectors.toMap(d -> d.getKey(), d -> d.getValue()));
        } else {
            maps = TMSlangWord.entrySet().stream().filter(data -> data.getValue().stream().filter(d -> d.toLowerCase().indexOf(text.toLowerCase()) != -1).count() > 0).collect(Collectors.toMap(d -> d.getKey(), d -> d.getValue()));
        }

        if (maps.size() == 0) {
            return null;
        }

        return maps;
    }

    public static int randomIndex(int minimum, int maximum) {
        return (minimum + (int) (Math.random() * maximum));
    }
}
